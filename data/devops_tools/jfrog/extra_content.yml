pages:
  - path: jfrog-pipelines
    content:
      title:  JFrog Pipelines vs Gitlab
      description: JFrog Pipelines product compared with GitLab. Highlights commonalities and gaps between products.
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: JFrog Pipelines Analysis
      css: extra-content-devops-tools.css
      page_body: |
        ## On this page
        {:.no_toc}

        - TOC
        {:toc}

        JFrog Pipelines, through acquisition of Shippable, is a functional CI-CD product. JFrog Pipelines attempts to make it simpler to do CI-CD by building 'Native Steps'. This is akin to a prebuilt component or step in the CI-CD process that can be described in Yaml, thereby hiding all the low level complexity from the user. Some examples of Native Steps are Docker Build, Docker Push, NPM Build, NPM Publish, and XrayScan. JFrog Pipelines has several strengths and weaknesses. The main impact of its weaknesses are longer build times and lower collaboration.

        ## JFrog Pipelines Strengths

        - Tight integration with Artifactory.
        - Ability to mix and match Native Steps with Custom Code - which reduces out of the box effort but provides flexibility to customize.
        - Well architected with core concepts of Steps (Native & Custom), Resources, Pipelines which can all be reused.

        ## JFrog Pipelines Gaps

        - Lacks enterprise grade features such as AutoDevOps - ability to recognize the code and pre-build a pipeline.
        - No concept of a Merge Request or a container like object that enables multiple developers to easily collaborate.
        - Cannot self-optimize builds and pipelines when a queue of submissions are made.
        - Lack of tight integration with testing - i.e. there is no native step that drives tests.
        - Lack of scalability of testing during the build process through innovative use of parent-child pipelines that can run in parallel.

        ## Similarities between JFrog Pipelines and Gitlab

        | Capability                    | Description                                                                                                                                                   |
        |-------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
        | YAML Based                    | Pipelines use YAML Syntax                                                                                                                                     |
        | Reusable Steps                | Can daisy chain steps and reuse steps in multiple different contexts.  Native or out of box steps are atomic.                                                 |
        | Scales Horizontally           | Scales horizontally.  As it grows u can add nodes into your system.  Scales horizontally to support 1000s of apps. One tool for all OSs, elastic build nodes. |
        | Integrates within their suite | The recent JFrog Platform product is well integrated.  Pipelines integrates with Artifactory and Xray.                                                        |

        ## GitLab Capabilities missing in JFrog Pipelines

        | Capability                          | Description                                                                                                                                                                                                                                                                                                                                                                                                          |
        |-------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
        | Merge Requests                      | Merge Requests are an organizing entity that capture facilitate all notes, actions and activity needed to drive change.  It also developers collaborate around changes.  This capability is missing in JFrog Pipelines, forcing customers to rely on third party products to bridge this gap.                                                                                                                        |
        | Merge Trains                        | Merge Trains enable users to schedule, manage and optimize builds.  This feature is very useful when many distributed developers contribute code to the same project.                                                                                                                                                                                                                                                |
        | Auto DevOps                         | AutoDevOps recognizes the source code language and automatically sets up the pipelines necessary to build and deploy.  This saves time to build and maintain pipelines.  Smart capability such as this is missing in JFrog Pipelines.                                                                                                                                                                                |
        | Parent child pipelines              | Parent-Child pipelines capability enables multiple parallel builds.  These builds are self orchestrated and enables several capabilities such as - parallelizing testing, building and testing a matrix of conditions etc.  JFrog is missing this critical capability.                                                                                                                                               |
        | Review Apps                         | Review Apps allow developers to quickly review the changes and approve.  This prevents need for traditionally expensive dev-test cycles.  Furthremore, it also enables distributed teams to review changes and take action.  This capability is provided as a standard part of GitLab's CI-CD solution.  JFrog does not support Review Apps. Customers need additional software and infrastructure when using JFrog. |
        | Feature Flags                       | Feature Flags allow various deployment models such as Blue-Green deployments to test changes against a small sub-set of customers before making it widely available. This capability is missing in JFrog and requires purchase of additional third party software licenses.                                                                                                                                          |
        | Developer productivity features     | Developer Productivity is a key driver for DevOps adoption. GitLab increases developer productivity through features such as kicking off builds via comments in Merge Requests.  These capabilities are currently missing in JFrog.                                                                                                                                                                                  |
        | Integration to Testing and Security | GitLab's integration to Security software and testing is seamless.  JFrog has integrated separate products into a single platform.  The user experience is not as seamless as in GitLab.                                                                                                                                                                                                                             |

  - path: jfrog-xray
    content:
      title: JFrog X-Ray and GitLab Security Comparison
      description: The differences and similarities between GitLab Security and JFrog X-Ray capabilities.
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: JFrog X-Ray Comparison
      css: extra-content-devops-tools.css
      page_body: |

        JFrog Xray provides static application testing capabilities by scanning the application components for vulnerabilities against the VulnDB vulnerability database. Xray also provides security policy enforcement and capability to monitor for license compliance. Xray integrates with IDEs such as IntelliJ and allows developers to view security issues in the dev environment.

        ## Summary Comparison

        | Capability          | GitLab | JFrog X-Ray |
        |---------------------|--------|-------------|
        | SAST                | Yes    | Yes         |
        | Dependency Scanning | Yes    | Yes         |
        | Container Scanning  | Yes    | Yes         |
        | License Compliance  | Yes    | Yes         |
        | Auto Remediation    | Yes    | Yes         |
        | DAST                | Yes    | **No**      |
        | Secrets Detection   | Yes    | **No**      |
        | API Fuzzing         | Yes    | **No**      |
        | Coverage Fuzzing    | Yes    | **No**      |
        | IAST                | **No** | **No**      |

        ## JFrog X-Ray Strengths

        - Security scanning during development and after binaries are built.
        - Ability to restrict downloads of artifacts deemed not in compliance with license or security policies.

        ## JFrog X-Ray Gaps

        - Dynamic Application Security Testing. Xray does not extend into the post deployment phase.
        - Cannot detect secrets within code.
        - Developers have to go back and forth from the IDE to Xray UI to manage security.

        ## Similarities between JFrog X-Ray and GitLab

        | Capability         | Description                                                                                                                                                                                                                       |
        |--------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
        | SAST               | Xray provides SAST capabilities and integrates with other IDEs.  It uses VulnDB for security vulnerability information                                                                                                            |
        | Container Scanning | Xray scans containers and binaries by unpacking the dependency information.                                                                                                                                                       |
        | Policy Enforcement | Can define Security and License policies.  Rules are added with defined criteria.   When a condition is met it can trigger a webhook for response. X-Ray also has built in capability to prevent downloads that violate policies. |
        | License Compliance | Licensing policies for software components.  X-Ray will identify and trigger a notification/response.                                                                                                                             |

  - path: jfrog-artifactory
    content:
      description: High level summary of JFrog Artifactory including strengths and challenges.
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: JFrog Artifactory Analysis
      css: extra-content-devops-tools.css
      page_body: |

        ## Overview

        JFrog Artifactory is a repository manager that is designed to store a wide range of artifacts needed for deployment and distribution during the Software Development Lifecycle.  Users can deploy Artifactory on-prem (self-hostd), in the cloud (SaaS) or in a hybrid model.  The three repository types that can be hosted in Artifactory are:

        * Local: physical, locally-managed repositories into which you can deploy artifacts
        * Remote: serves as a caching proxy for a repository managed at a remote URL (which may itself be another Artifactory remote repository)
        * Virtual: aggregates several repositories with the same package type under a common URL

        Artifactory also stores a complete map of all the components that went into creating the artifact.  This information feeds other products such as JFrog Xray.

        ## Kubernetes

        Kubernetes applications are built from many types of artifacts sourced from multiple locations.  JFrog’s centralized approach to artifact storage addresses challenges of combining variously sourced components for kubernetes containerized applications.
        Artifactory can also serve as a Kubernetes Registry allowing the traceability of dependencies across Docker images.

        ## JFrog Artifactory vs. GitLab Package Registry Comparison

        JFrog’s approach to managing and storing artifacts is to use one universal repository manager for housing all packet formats in  a central location.  GitLab’s goal is for you to rely on GitLab as a universal package manager, so that you can reduce costs and drive operational efficiencies.

        * **Remote and Virtual Repositories**: A critical gap between Artifactory and GitLab's Package offering is the ability to easily connect to and group external, remote registries.  We plan on bridging this gap by expanding the Dependency Proxy to support remote and virtual registries.
        * **Supported Packages**: The table below lists JFrog vs GitLab's supported package manager formats. Artifactory support a longer list of formats, but we have not heard many requests from our customers for some of these formats. If you'd like to suggest we consider a new format, please open an issue [here](https://gitlab.com/gitlab-org/gitlab/-/issues). To date, GitLab has been focused on delivering Project and Group-level private package registries for the most commonly used formats.

        In addition, GitLab strengths are in providing a single product for the full DevOps Lifecycle.  GitLab CI-CD and Security Capabilities have better functionality and provide enterprise grade capabilities.

        For more details on how GitLab is evolving our Package Managment solution please see our [Category Direction - Package Registry Page](https://about.gitlab.com/direction/package/#universal-package-management-tools).

        ### **_JFrog vs. GitLab Feature Comparison_**

        |                                                                                                                                           	|      GitLab     	| JFrog 	|
        |-------------------------------------------------------------------------------------------------------------------------------------------	|:---------------:	|:-----:	|
        | **Dependency Management**<br>A version-controlled common library can be shared by all development teams                                   	|        ✅        	|   ✅   	|
        | **Efficient Builds**<br>Easy to access artifacts that are cached locally once downloaded                                                  	|        ✅        	|   ✅   	|
        | **Release Stability**<br>Artifacts and metadata stay stable after being published to a release repository                                 	|        ✅        	|   ✅   	|
        | **Artifact Traceability**<br>Versions are tracked                                                                                         	|        ✅        	|   ✅   	|
        | **Package Format**<br>Wide support for package formats such as package formats such as Maven, Debian, NPM, Helm, Ruby, Python, and Docker 	|   🟨<br>Partial  	|   ✅   	|
        | **Repository Variety**<br>Supports local, remote and virtual repositories                                                                 	| 🟨<br>Local Only 	|   ✅   	|

        ### **_JFrog vs. GitLab Supported Packages Comparison_**

        |              	|         JFrog         	|         GitLab        	|
        |--------------	|:---------------------:	|:---------------------:	|
        | Alpine       	|           ✅           	|           ❌           	|
        | Bower        	|           ✅           	|           ❌           	|
        | Chef         	|           ✅           	|           ❌           	|
        | CocoaPods    	|           ✅           	|           ❌           	|
        | Conan        	|           ✅           	|           ✅           	|
        | Conda        	|           ✅           	|           ❌           	|
        | CRAN         	|           ✅           	|           ❌           	|
        | Debian       	|           ✅           	|           ❌           	|
        | Docker       	|           ✅           	|           ✅           	|
        | Git LFS      	|           ✅           	|           ❌           	|
        | Go Registry  	|           ✅           	|           ✅           	|
        | Helm         	|           ✅           	|           ✅           	|
        | Maven        	|           ✅           	|           ✅           	|
        | NPM          	|           ✅           	|           ✅           	|
        | NuGet        	|           ✅           	|           ✅           	|
        | Opkg         	|           ✅           	|           ❌           	|
        | P2           	|           ✅           	|           ❌           	|
        | PHP Composer 	|           ✅           	|           ✅           	|
        | Puppet       	|           ✅           	|           ❌           	|
        | PyPI         	|           ✅           	|           ✅           	|
        | RPM          	|           ✅           	|           ❌           	|
        | RubyGems     	|           ✅           	|           ❌           	|
        | SBT          	|           ✅           	|           ❌           	|
        | Vagrant      	|           ✅           	|           ❌           	|
        | VCS          	|           ✅           	|           ❌           	|


        ## Artifactory Gaps

        JFrog Artifactory is an industry leader in artifact managent and does not have major gaps.  User feedback indicates Artifactory is difficult to set up and manage.



  - path: jfrog-pricing
    content:
      title: JFrog Pricing Model Analysis
      description: Analysis of the pros and cons of the JFrog Pricing Model and implications for DevOps teams.
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: JFrog Pricing Analysis
      css: extra-content-devops-tools.css
      page_body: |
        ## Summary

        GitLab has a user based pricing model for its on-premises versions and on its SaaS version.  The GitLab SaaS version has some consumption based elements.   In contrast to GitLab, JFrog uses a consumption based pricing both for its on-premises and cloud based versions.
        Here's some additional information on the main differences in JFrog's pricing models.

        ## JFrog Pricing Model Characteristics

        |                           |                                                                                                                                                                                                                                                                                               |
        |---------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
        | **Consumption Based Pricing** | JFrog pricing has consumption based elements in both its Cloud and On-Prem versions. In Cloud consumption factors are Build Minutes, Storage and Transfer rates. Whereas in On-Prem version the consumption factor is number of concurrent builds that can be run.                            |
        | **Unlimited Users and Roles** | JFrog does not limit based on number of users or number of repositories. This is likely a function of <br> (a) Has to integrate with other products such as SCM that are largely user based and <br> (b) (b) A legacy of their Artifactory pricing, which did not account for users or repos. |

        ## Challenges in JFrog Pricing Model

        | Challenge                                                        | Explanation                                                                                                                                                                                                           |
        |------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
        | **Lack of Predictability**                                           | Consumption based pricing allows the pricing to fluctuate based on usage,hence the expenditures are not predictable                                                                                                   |
        | **Pricing Model Penalizes Customers for Using DevOps Best Practice** | Smaller changes, more frequent builds, more testing etc.  In each of these scenarios consumption based pricing will cause users to spend more.  Hence adopting DevOps best practices becomes expensive for customers. |
        | **Bundling Inconsistencies**                                         | Xray is available in a lower priced Pro-X tier, not available with Enterprise, which is the next tier product, again available in Enterprise +.                                                                       |

        ## JFrog Cloud Pricing Model

        | JFrog Pro                                                                                                                | JFrog Pro-X                                                                                                                   | JFrog Enterprise                                                                                                  | JFrog Enterprise+                                                                                                                                                                                                                                                                                      |
        |--------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
        | -  JFrog Artifactory<br>-  JFrog Pipelines                                                                               | - JFrog Artifactory<br>- JFrog Xray<br>- JFrog Pipelines                                                                      | - JFrog Artifactory<br>- JFrog Pipelines<br>- JFrog Xray(Add-on)<br>- Multi-site Replication<br>- Artifactory CDN | - JFrog Artifactory<br>- JFrog Pipelines<br>- JFrog Xray(Add-on)<br>- JFrog Mission Control<br>- JFrog Distribution<br>- JFrog Artifactory Edge Nodes<br>- JFrog Federation<br>- JFrog Insight<br>- Multi-site Replication<br>-Artifactory CDN<br>- On-site training<br>- High-touch dedicated support |
        | - $98/mo or $150/mo with 5k build min of Pipelines<br>- $0.008 per build min for overages<br>- 2GB/10GB Storage/Transfer | - $499/mo or $599/mo with 15k build min of Pipelines<br>- $0.007 per build min for overages<br>- 125GB/200GB Storage/Transfer | - Xray and Pipelines are add-ons for custom price<br>- Custom storage and transfer limits                         | - Custom Price<br>- Custom Storage and Transfer Limits                                                                                                                                                                                                                                                 |
        |                                                                                                                          | Imputed JFrog Xray Price - $400/month                                                                                         | Imputed price for multi-site and CDN is $600/mo.                                                                  |                                                                                                                                                                                                                                                                                                        |

        ## JFrog On-Premises Pricing Model

        | **JFrog Pro**     | **JFrog Pro-X**                       | **JFrog Enterprise**                                                                                                    | **JFrog Enterprise+**                                                                                                                                                                                                                                                                                                                                                                               |
        |-------------------|---------------------------------------|-------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
        | JFrog Artifactory | - JFrog Artifactory<br>- JFrog Xray   | - JFrog Artifactory<br>- JFrog Mission Control<br>- High Availability<br>- Multi-site Replication<br>- Storage Sharding | - JFrog Artifactory<br>- JFrog Pipelines*<br>- JFrog Xray<br>- JFrog Mission Control<br>- JFrog Artifactory Edge Nodes<br>- JFrog Distribution<br>- JFrog Federation<br>- JFrog Insight<br>- High Availability<br>- Multi-site Replication<br>- Storage Sharding<br>- On-site training<br>- High-touch dedicated support<br>* 15 concurrent builds, Additional build agents are $4880 for 10pak |
        | $2950/year        | $14,400/year                          | $29,500/year                                                                                                            | Custom Price                                                                                                                                                                                                                                                                                                                                                                                    |
        |                   | Imputed price for Xray is 11,450/year | Imputed price for HA, Replication etc. is $26550                                                                        | Estimated baseline price is $40k per year                                                                                                                                                                                                                                                                                                                                                       |
